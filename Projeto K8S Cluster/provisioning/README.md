# Provisioning

Essa fase consiste em somente criar a infra estruta na aws.

Apesar de não ser necessário o aws-cli instalado na máquina, nessa fase do projeto é como se o ansible de fato fosse o nosso aws-cli. Ele irá precisar das suas credenciais da aws para ir lá e mandar criar algo pra na cloud.

Diferente do terraform, o ansible não utiliza state para guardar o estado. Por isso costumo dizer que para esse cenário o terraform seria melhor opção.

## hosts

o arquivo hosts tem um peculiaridade nessa parte do projeto, pois nós não teremos um host para atuar em cima, dessa forma ele é local.
Veja o arquivo hosts desse projeto e compreenda um pouco melhor.

Aqui já introduzimos de variaveis para diminuir o tamanho do nosso main.

O bloco [kubernetes] fica vazio, pois nao temos ainda os ips dos nossos hosts ali, e ele serã preenchido automágicamente com os ips das máquinas que criaremos. Esses ips devem ser usados na segunda etapa do projeto que é o setup.

## main.yml

Este arquivo somente tem a função de chamar as nossas roles. Nesse casol só teremos uma role que é a de montar criar a infra que será utilizada no segundo passo, que é o setup, para configurá-la. \
Nesse momento ainda não temos pre tasks, logo vamos concentrar sõ em roles mesmo.
<!-- aqui vale uma melhoria de alimentar tb os hosts do setup e dividir entre ip publico e privado além de ja saber quem é master quem é workers -->

o nosso arquivo main poderia ser assim

```yml
- hosts: local
  ansible_python_interpreter: python
  ansible_connection: local 
  gather_facts: false # nao trazer os facts nesse momento pois nao tem valia

  roles:
  - create-instances
```

Porém aprendendo um pouco mais, essas variáveis podem fazer parte do hosts caso quisermos, e optamos por essa opção.
